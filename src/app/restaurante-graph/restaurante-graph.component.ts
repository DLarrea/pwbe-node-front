import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MesaService } from '../services/mesa.service';
import { RestauranteService } from '../services/restaurante.service';

declare var Xy: any;
@Component({
  selector: 'app-restaurante-graph',
  templateUrl: './restaurante-graph.component.html',
  styleUrls: ['./restaurante-graph.component.css']
})
export class RestauranteGraphComponent implements OnInit {

  id = null;
  restaurant = null;
  mesas = null;
  capacidadTotal = 0;
  plantas = [];
  plantasMesas = [];
  loading = true;

  constructor(private route: ActivatedRoute, private apiResto: RestauranteService, private apiMesa: MesaService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getRestaurante();
      this.getMesas();
    });

  }

  ready = () => {
    this.loading = false;
  }

  getRestaurante = () => {
    this.apiResto.get(this.id).subscribe(
      data => {
        this.restaurant = data;
        console.log(this.restaurant);
        this.restaurant && this.mesas && this.ready();
      }
    )
  }

  getMesas = () => {
    this.apiMesa.getAll(this.id, null)
      .subscribe(
        data => {
          this.mesas = data;
          console.log(this.mesas);
          this.restaurant && this.mesas && this.ready();
          this.setPlantas();
        },
        error => {
          console.log(error);
        });
  }

  setPlantas = () => {
    for (let i = 0; i < this.mesas.length; i++) {
      let el = this.mesas[i];
      this.capacidadTotal += el.capacidad;
      if (!this.plantas.includes(el.planta)) {
        this.plantas.push(el.planta);
      }
    }
    this.plantas.sort();
    this.setPlantasMesas();

  }

  setPlantasMesas = () => {

    for (let i = 0; i < this.plantas.length; i++) {
      let planta = this.plantas[i];
      let mesaPlanta = {
        planta: planta,
        mesas: [],
        canvaSelector: `sigma-${planta}`,
        xyItems: [],
        el: null,
        ctx: null,
        xy: null
      };
      for (let j = 0; j < this.mesas.length; j++) {
        let mesa = this.mesas[j];
        if (mesa.planta == planta) {
          mesaPlanta.mesas.push(mesa);
          mesaPlanta.xyItems.push([mesa.posicionx, mesa.posiciony]);
        }
      }
      this.plantasMesas.push(mesaPlanta);
    }
    console.log(this.plantasMesas);
    setTimeout(() => {
      this.draw();
    }, 500);
    // this.draw();
  }

  draw = () => {
    
    for (let i = 0; i < this.plantasMesas.length; i++) {
      
      let pm = this.plantasMesas[i];
      var datasets = [
        {
          pointColor: 'rgba(0, 123, 255, 1)',
          data: []
        }
      ];
      for(let i=0;i < pm.xyItems.length;i++){
        let p = pm.xyItems[i];
        datasets[0].data.push(p);
      }
      console.log(datasets);
      pm.el = <HTMLCanvasElement> document.getElementById(pm.canvaSelector);
      pm.ctx = pm.el.getContext('2d')
      pm.xy = new Xy(pm.ctx, { line: false, rangeX: [0, 'auto'], rangeY: [0, 'auto'], tickStepX: 1, tickStepY: 1 });
      pm.xy.draw(datasets);
      
    }

  }

}
