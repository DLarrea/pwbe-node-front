import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestauranteGraphComponent } from './restaurante-graph.component';

describe('RestauranteGraphComponent', () => {
  let component: RestauranteGraphComponent;
  let fixture: ComponentFixture<RestauranteGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestauranteGraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestauranteGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
