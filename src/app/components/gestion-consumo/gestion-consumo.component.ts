import { Component, OnInit } from '@angular/core';
import { Consumo } from 'src/app/models/consumo';
import { ClienteService } from 'src/app/services/cliente.service';
import { ConsumoService } from 'src/app/services/consumo.service';
import { jsPDF } from "jspdf";
import { DatePipe } from '@angular/common';

declare var $:any;
@Component({
  selector: 'app-gestion-consumo',
  templateUrl: './gestion-consumo.component.html',
  styleUrls: ['./gestion-consumo.component.css']
})
export class GestionConsumoComponent implements OnInit {

  constructor(private apiConsumo: ConsumoService, private apiCliente: ClienteService, private dp: DatePipe) { }

  ngOnInit(): void {
    this.getMesas();
    this.getClientes();
    this.getProductos();
  }

  mesas = [];
  consumos = []; 
  clientes = [];
  productos = [];
  addCliente = {
    cedula: null,
    nombre: null,
    apellido: null
  }
  nuevoCliente = null; 
  mesa = null;
  ready = false;

  getMesas = () => {
    this.apiConsumo.getMesas().subscribe(
      data => {
        this.mesas = data;
        this.mesas.forEach(el => {
          el["search"] = `${el.nombre} / ${el.Restaurante.nombre}`;
        });
        console.log(this.mesas);
      }
    )
  }
  
  getProductos = () => {
    this.apiConsumo.getProductos().subscribe(
      data => {
        this.productos = data;
        this.productos.forEach(el => {
          el["search"] = `${el.nombre} / PYG ${el.precio}`;
        });
        console.log(this.productos);
      }
    )
  }
  
  getClientes = () => {
    this.apiCliente.getAll(null).subscribe(
      data => {
        this.clientes = data;
        this.clientes.forEach(el => {
          el["search"] = `${el.nombre} ${el.apellido} Nro. CI ${el.cedula}`;
        })
        console.log(this.clientes);
      }
    )
  }

  buscarConsumos = () => {
    if(this.mesa != null){
      this.ready = false;
      this.apiConsumo.getConsumoByMesa(this.mesa).subscribe(data => {
        this.consumos = data;
        this.ready = true;
        console.log(this.consumos);
      });
    }
  }

  target = null;
  targetConsumo: Consumo = new Consumo;
  setModalCliente = (target) => {
    this.target = target;
    this.targetConsumo.id = this.target.id;
    this.targetConsumo.cliente = this.target.ClienteId;
    this.targetConsumo.mesa = this.target.MesaId;
    this.targetConsumo.cierreConsumo = this.target.cierreConsumo;
    this.targetConsumo.creacionConsumo = this.target.creacionConsumo;
    this.targetConsumo.estado = this.target.estado;
  }

  createOrSelectCliente(){
    if(this.addCliente.nombre != null && this.addCliente.apellido != null && this.addCliente.cedula != null){
      this.guardarCliente();
    } else {
      this.actualizarConsumo();
    }
  }

  guardarCliente(){
    this.apiCliente.create(this.addCliente).subscribe(
      data => {
        this.addCliente = {
          nombre: null,
          apellido: null,
          cedula: null
        }
        this.nuevoCliente = data.id;
        this.actualizarConsumo();
      }
    )
  }


  actualizarConsumo(){
    if(this.nuevoCliente != null){
      this.targetConsumo.cliente = this.nuevoCliente;
      this.apiConsumo.putConsumo(this.targetConsumo).subscribe(
        data => {
          this.buscarConsumos();
        }
      )
    }
  }

  agregarDetalleObj = {
    producto: null,
    cantidad: null,
    cabecera: null,
    total: null,
    productoPrecio: null
  }

  setModalAgregarDetalle(target){
    this.agregarDetalleObj.total = target.total;
    this.agregarDetalleObj.cabecera = target.id;
    console.log(this.agregarDetalleObj);
  }

  changeProductoAgregarDetalle(){
    this.agregarDetalleObj.productoPrecio = this.agregarDetalleObj?.producto?.precio;
    console.log(this.agregarDetalleObj);
  }

  agregarDetalle(){
    this.agregarDetalleObj.producto = this.agregarDetalleObj.producto?.id;
    this.apiConsumo.agregarDetalle(this.agregarDetalleObj).subscribe(
      data => {
        this.buscarConsumos();
        this.agregarDetalleObj = {
          producto: null,
          cantidad: null,
          cabecera: null,
          total: null,
          productoPrecio: null
        }
      }
    )
  }

  deleteDetalle(targetCabecera, targetDetalle){
    let total = targetCabecera.total - targetDetalle?.cantidad * targetDetalle?.Producto?.precio;
    console.log(total);
    this.apiConsumo.deleteDetalle(targetDetalle.id, targetCabecera.id, total).subscribe(
      data => {
        this.buscarConsumos();
      }
    )
  }

  cerrarConsumo(target){
    let fin = this.dp.transform(new Date(), "yyyy-MM-dd HH:mm:ss");
    let body = {
      id: target.id,
      estado: "C",
      cierreConsumo: fin
    }

    this.apiConsumo.cerrarConsumo(body).subscribe(
      data => {
        this.ticket(target, fin);
        this.buscarConsumos();
      }
    )
  }

  ticket(target, fin){
    const doc = new jsPDF();
    let width = doc.internal.pageSize.getWidth();
    doc.setFontSize(18);
    doc.text("Ticket - Resto App", 10, 10);
    doc.setFontSize(12);
    doc.text(`${target?.Cliente.nombre} ${target?.Cliente.apellido}`, 10, 18);
    doc.text(`Nro. CI ${target?.Cliente.cedula}`, 10, 26);
    doc.text(`Hora de inicio ${target?.creacionConsumo}`, 10, 34);
    doc.text(`Hora de finalizacion ${fin}`, 10, 42);
    doc.line(10, 50, width - 10, 50);

    doc.setFontSize(16);
    doc.text("Detalles", 10, 58);
    doc.setFontSize(10);
    
    let h = 66;
    let inc = 6;
    for(let i=0; i<target.detalles.length;i++){
      let det = target.detalles[i];
      doc.text(`${det?.Producto?.nombre} / Precio Unit.: ${det?.Producto?.precio} / Can.: ${det?.cantidad} / Subtotal: PYG ${det?.cantidad * det?.Producto?.precio}`, 10, h);
      h += inc;
    }
    h += 4;
    doc.line(10, h, width - 10, h);
    doc.setFontSize(16);
    h += 12;
    doc.text(`Total PYG ${target?.total}`, 10, h);
    doc.save(`tk_${this.dp.transform(new Date(), 'dd_MM_yyy_HH_mm_ss')}_${target?.Cliente?.cedula}`);
  }

  nuevoDetalleCliente = {
    id: null,
    nombre: null,
    apellido: null,
    cedula: null
  }

  nuevoDetalleProductos = [];
  nuevoDetalleProducto = {
    producto: null,
    cantidad: null,
    productoPrecio: null
  }

  guardarDetalle(){
    // this.nuevoDetalleProducto.producto = this.nuevoDetalleProducto.producto.id;
    this.nuevoDetalleProductos.push({...this.nuevoDetalleProducto});
    this.nuevoDetalleProducto = {
      producto: null,
      cantidad: null,
      productoPrecio: null
    }
  }

  guardarNuevoCliente(){
    
    this.addCliente.cedula = Number(this.addCliente.cedula);
    this.apiCliente.create(this.addCliente).subscribe(
      data => {
        this.addCliente = {
          nombre: null,
          apellido: null,
          cedula: null
        }
        this.nuevoDetalleCliente = data;
      }
    )
  }

  changeNuevoProductoAgregarDetalle(){
    this.nuevoDetalleProducto.productoPrecio = this.nuevoDetalleProducto?.producto?.precio;
    console.log(this.nuevoDetalleProducto);
  }

  guardarConsumo(){

    let detalles = [...this.nuevoDetalleProductos];

    if(detalles.length == 0 || this.nuevoDetalleCliente == null) return;

    for(let i=0;i<detalles.length;i++){
      let det = detalles[i];
      det.producto = det.producto.id;
    }

    let obj = {
      cabecera: {
        cliente: this.nuevoDetalleCliente.id,
        creacionConsumo: this.dp.transform(new Date(), 'yyyy-MM-dd HH:mm:ss'),
        mesa: this.mesa
      },
      detalles: detalles
    }
    console.log(obj);
    this.apiConsumo.postConsumo(obj).subscribe(
      data => {
        this.buscarConsumos();
        this.nuevoDetalleCliente = {
          id: null,
          nombre: null,
          apellido: null,
          cedula: null
        }
        this.nuevoDetalleProductos = [];
      }
    )
  }
}
