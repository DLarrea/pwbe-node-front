import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrl = 'http://localhost:8080/api/consumo';
const baseUrlProducto = 'http://localhost:8080/api/producto';

@Injectable({
  providedIn: 'root'
})
export class ConsumoService {

  constructor(private http: HttpClient) { }

  getMesas(): Observable<any> {
    return this.http.get(`${baseUrl}/mesas`);
  }
  
  
  getConsumoByMesa(idMesa): Observable<any> {
    let httpParams = new HttpParams();
    if(idMesa){
      httpParams = httpParams.append("mesa", idMesa.toString());      
    }
    return this.http.get(`${baseUrl}`, {params: httpParams});
  }
  
  
  putConsumo(body): Observable<any> {
    let b = {
      ClienteId: body.cliente
    }
    return this.http.put(`${baseUrl}/${body.id}`, b);
  }
  
  postConsumo(body): Observable<any> {
    return this.http.post(`${baseUrl}`, body);
  }
 
  cerrarConsumo(body): Observable<any> {
    return this.http.put(`${baseUrl}/${body.id}`, body);
  }
  
  agregarDetalle(body): Observable<any> {
    return this.http.post(`${baseUrl}/detalle`, body);
  }

  deleteDetalle(id, cabecera, total): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append("cabecera", cabecera.toString());      
    httpParams = httpParams.append("total", total.toString());      
    return this.http.delete(`${baseUrl}/detalle/${id}`, {params:httpParams});
  }

  getProductos(): Observable<any> {
    return this.http.get(`${baseUrlProducto}`);
  }
}
